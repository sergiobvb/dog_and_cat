﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DogHealth : MonoBehaviour
{
    public int dogHealth;
    public Image image;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "CatBullet")
        {
            dogHealth -= 10;
            image.fillAmount -= 0.1f;
        }
    }
}
