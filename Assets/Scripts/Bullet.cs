﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Bullet : MonoBehaviour
{

    void OnTriggerEnter2D(Collider2D collider)
    {
        //Debug.Log("BOOM!");
       
        if(collider.tag=="Ground"||collider.tag=="Player"||collider.tag=="House")
        {
            Destroy(gameObject);
        }
    }
 
}
