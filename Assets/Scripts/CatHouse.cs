﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CatHouse : MonoBehaviour
{
    public Sprite house01;
    public Sprite house02;
    public Sprite house03;
    public Sprite house04;
    private int count = 5;

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag == "DogBullet")
        {
            --count;
            switch (count)
            {
                case 4:
                    gameObject.GetComponent<SpriteRenderer>().sprite = house01;
                    Debug.Log(count);
                    break;
                case 3:
                    gameObject.GetComponent<SpriteRenderer>().sprite = house02;
                    break;
                case 2:
                    gameObject.GetComponent<SpriteRenderer>().sprite = house03;
                    break;
                case 1:
                    gameObject.GetComponent<SpriteRenderer>().sprite = house04;
                    break;
                case 0:
                    Destroy(gameObject);
                    break;
            }
        }
    }
}
