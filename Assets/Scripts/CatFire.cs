﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class CatFire : MonoBehaviour
{
    public RectTransform Canvas;
    public Transform LaunchAngle;

    public float Power = 10;
    public float Gravity = -10;

    public GameObject bullet;
    public Transform shotSpawn;
    public Transform childTransform;

    public Image img;

    private Vector3 MoveSpeed;
    private Vector3 GritySpeed = Vector3.zero;
    private float dTime;
    private Vector3 currentAngle;
    private int currentChildrenCount;

    private bool isUp;
    private float LimitZ;

    private TurnControl turnControl;
    public GameObject Turn;

    private Timer timer;
    public GameObject Timer;

    public Transform Cam;

    public GameObject Dog;
    public GameObject Cat;
    public GameObject DogCanvas;
    public GameObject CatCanvas;

    private bool isGenerate = false;

    void Start()
    { 
        EventTriggerListener.Get(gameObject).onDown += OnClickDown;
        EventTriggerListener.Get(gameObject).onUp += OnClickUp;
        turnControl = Turn.GetComponent<TurnControl>();
        timer = Timer.GetComponent<Timer>();
    }
    void OnClickDown(GameObject go)
    {
        isUp = false;
        StartCoroutine(grow());
    }

    void OnClickUp(GameObject go)
    {
        if (img.fillAmount!=1)
        {
            Instantiate(bullet, shotSpawn.position, shotSpawn.rotation, shotSpawn);
            InitializationBullet();
        }
        isUp = true;
        img.fillAmount = 0f;
    }

    private IEnumerator grow()
    {
        while (true)
        {
            if (isUp)
            {
                break;
            }
            img.fillAmount += 0.003f;
        
            if (img.fillAmount == 1)
            {
                Instantiate(bullet, shotSpawn.position, shotSpawn.rotation,shotSpawn);
                InitializationBullet();
                isUp = true;
                break;
            }
            yield return null;
        }
    }

    private Vector3 bulletCurrentPosition;
    // Update is called once per frame
    void FixedUpdate()
    {
        currentChildrenCount = shotSpawn.childCount;
        if(turnControl.isCatAction == true)
        {
            Dog.GetComponent<DogControl>().enabled = false;
            Cat.GetComponent<CatControl>().enabled = true;

            DogCanvas.SetActive(false);
            if (CatCanvas.activeInHierarchy == false)
            {
                CatCanvas.SetActive(true);
            }

            if (currentChildrenCount != 0)
            {
                //m=gt;
                //v = at ;
                bulletCurrentPosition = childTransform.position;
                GritySpeed.y = Gravity * (dTime += Time.fixedDeltaTime);

                childTransform.position += (MoveSpeed + GritySpeed) * Time.fixedDeltaTime;

                Cat.GetComponent<CatControl>().enabled = false;
                Cam.position = new Vector3(childTransform.position.x - bulletCurrentPosition.x + Cam.position.x, Cam.position.y, Cam.position.z);

                currentAngle.z = Mathf.Atan((MoveSpeed.y + GritySpeed.y) / MoveSpeed.x) * Mathf.Rad2Deg;
                childTransform.eulerAngles = currentAngle;


                isGenerate = true;           
            }

            if(currentChildrenCount==0&&isGenerate==true||timer.count==0)
            {
                //the dog`s turn
                timer.count = 10.0f;
                turnControl.isDogAction = true;
                turnControl.isCatAction = false;
                Cam.position = new Vector3(-1.0f, Cam.position.y, Cam.position.z);
                isGenerate = false;
                DogCanvas.SetActive(true);      
            }
        }   
    }


    private void Update()
    {
        Vector3 mousePos;
        RectTransformUtility.ScreenPointToWorldPointInRectangle(Canvas, new Vector2(Input.mousePosition.x, Input.mousePosition.y), Camera.main, out mousePos);


        if (mousePos.x > LaunchAngle.position.x)
        {
            LimitZ = -Vector3.Angle(Vector3.up, mousePos - LaunchAngle.position);
        }
        else
        {
            LimitZ = Vector3.Angle(Vector3.up, mousePos - LaunchAngle.position);
        }

        LimitZ = Mathf.Clamp(LimitZ, 47.0f, 128.0f);
        LaunchAngle.localRotation = Quaternion.Euler(0, 0, LimitZ);
    }

    void InitializationBullet()
    {
        if (shotSpawn.childCount == 1)
        {
            dTime = 0.0f;
            currentAngle = Vector3.zero;
            Power = img.fillAmount * 20.0f;

            childTransform = shotSpawn.Find("CatBullet(Clone)");
            MoveSpeed = Quaternion.Euler(0, 0, LimitZ + 40) * Vector3.right * Power;
        }
    }

}
