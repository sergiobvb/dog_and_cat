﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CatHealth : MonoBehaviour
{
    public int catHealth;
    public Image image;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag=="DogBullet")
        {
            catHealth -= 10;
            image.fillAmount -= 0.1f;
        }
    }
}
