﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour
{
    public float count;
    public Text timer;

    private void Start()
    {
        count = 10.0f;
    }

    private void Update()
    {
        count -= Time.deltaTime;
        timer.text = ""+(int)count;
    }
}
