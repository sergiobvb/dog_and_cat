﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DogControl : MonoBehaviour
{
    public Transform Cam;

    private Vector3 CurrentPositin;
    private float Hor;
    private float LimtX;
    private float LimitMoveX;

    private void Update()
    {
        CurrentPositin = transform.position;
        Hor = Input.GetAxis("Horizontal");//AD
        transform.Translate(Vector3.right * Hor * Time.deltaTime * 7);

        LimitMoveX = transform.position.x;
        LimitMoveX = Mathf.Clamp(LimitMoveX, -6.0f, 4.0f);
        transform.position = new Vector3(LimitMoveX, transform.position.y, transform.position.z);

        LimtX = Cam.position.x + transform.position.x - CurrentPositin.x;
        LimtX = Mathf.Clamp(LimtX, -1.0f, 14.0f);
        Cam.position = new Vector3(LimtX, Cam.position.y, Cam.position.z);
    }

}
